# Atividade Técnica - Analista QA - Take
## Executando no seu computador.
#### 1-Passo:
Abra o terminal e dê um git clone no projeto
```sh
git clone https://Rubens_Matos@bitbucket.org/Rubens_Matos/test_take.git
```
#### 2-Passo:
```sh
Depois entre na pasta "test_take"
```
#### 3-Passo:
```sh
Agora rode o comando: bundle install
```
#### 4-Passo:
```sh
Para rodar o projeto basta usar o comando:
cucumber
  ou
bundle exec cucumber

```