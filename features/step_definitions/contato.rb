Dado("que eu acesse a página do receitas") do
  visit "/"
end

Quando("eu clicar na opão [Menu]") do
  @Contato.menu.click
end

Quando("Clicar em [Contato]") do
  @Contato.contato.click
end

Quando("preencher os campos da interface") do
  @Contato.campoName.set @nome
  @Contato.campoEmail.set  @email
  @Contato.campoComentario.set @comentario
end

Quando("acionar o comando [Enviar]") do
  @Contato.btnEnviar.click
end

Entao("o sistema envia a mensagem e exibe menagem {string}") do |msg|
  expect(page).to have_content msg
  sleep 10
end