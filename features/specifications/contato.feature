#language: pt
@hooks_contato
Funcionalidade: Validar aplicação de teste Take

Eu, como QA desejo realizar 
contato na aplicação de teste
da Take

Cenario: Enviar contato pelo portal

    Dado que eu acesse a página do receitas
    Quando eu clicar na opão [Menu]
    E Clicar em [Contato]
    E preencher os campos da interface 
    E acionar o comando [Enviar]
    Entao o sistema envia a mensagem e exibe menagem "A mensagem foi enviada"
