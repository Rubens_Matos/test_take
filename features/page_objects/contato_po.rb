class PaginaContato < SitePrism::Page

  element :menu, :id, "site-navigation"
  element :contato, :id, 'menu-item-7'
  element :campoName, :id, 'g2-name'
  element :campoEmail, :id, 'g2-email'
  element :campoComentario, :id, 'contact-form-comment-g2-comment'
  element :btnEnviar, :xpath, '//*[@id="contact-form-2"]/form/p/input[1]'
  #element :messg, :xpath, '//*[normalize-space(text()) and normalize-space(.)="Contact"])[1]/following::h3[1]'
end
